/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import servlet.Url;
import servlet.ModelView;

/**
 *
 * @author Hoby
 */
public class Test {
       String nom;
    String prenom;
 
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    
    @Url(nom="afficher")
    public ModelView test3(){
       ModelView result = new ModelView();
        result.setUrlRedirect("page2.jsp");
        result.getData().put("nom",nom);
        result.getData().put("prenom",prenom);
    
        return result;
    }
}
