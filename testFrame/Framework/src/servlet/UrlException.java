/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

/**
 *
 * @author Tiavina Ravaka
 */
public class UrlException extends Exception{
    public UrlException() {
    }
    /**
     * Constructs an instance of <code>UrlNotSupportException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public UrlException(String message) {
        super(message);
    }
}
