/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author Hoby
 */
class Utilitaire{
    static String retrieveUrlFromRawUrl(String rawUrl){
        return rawUrl.split("/")[rawUrl.split("/").length-1].split(".do")[0];
    }
    static HashMap<String,HashMap<String,String>> link(String[] listepkgs) throws Exception {
        HashMap<String,HashMap<String,String>> val = new HashMap<String,HashMap<String,String>>();
        for(String pkg : listepkgs){
            Vector<Class> listeClass;
            try {
                listeClass= getClass(pkg);
            } catch (Exception e) {
                continue;
            }
            for(Class cl : listeClass){
                Method[] listeMethod = cl.getDeclaredMethods();
                for(Method method : listeMethod ){
                    if(method.isAnnotationPresent(Url.class)){
                        HashMap<String,String> classMeth = new HashMap<>();
                        classMeth.put("class", cl.getName());
                        classMeth.put("method", method.getName());
                        val.put(method.getAnnotation(Url.class).nom(),classMeth);
                        
                    }
                    
                }
            }
        }
        return val;
    }
    public static Vector<Class> getClass(String namepkg)throws Exception{
        Vector<Class> liste = new Vector<Class>(1,1);
        URL root= Thread.currentThread().getContextClassLoader().getResource(namepkg.replace(".", "/"));
        File fichier= null;
        
        try{
            fichier = new File(root.getFile());
        } catch (NullPointerException e) {
            throw new Exception();
        }
        FilenameFilter fltr =new FilenameFilter(){
            @Override
            public boolean accept(File dir, String name) {
                try {
                   return name.endsWith(".class"); 
                } catch (Exception e) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                
                }
        }
        };
        File[] files= fichier.listFiles(fltr);
        for (File file : files) {
            String className = file.getName().replaceAll(".class$", "");
            Class<?> cls = Class.forName(namepkg+ "." + className);
            boolean cond = !cls.equals(FrontServlet.class)|| !cls.equals(ModelView.class);
            if(cond) {
                liste.add(cls);
            }
        }
        return liste;
    }

    public static void urlIsPresent(String url,HashMap<String, HashMap<String, String>> urlLink) throws UrlException{
        
       if(!urlLink.containsKey(url)){
           throw new UrlException("URL not supported");
           
       }
      
    
    }
}
